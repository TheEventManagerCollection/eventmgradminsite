from flask_wtf import FlaskForm

from wtforms import StringField, PasswordField, SubmitField
from wtforms.fields.html5 import EmailField

from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
	admin_id = StringField('Admin ID')
	password = PasswordField('Password')
	
	submit = SubmitField("Log In")
	
def validate_login_form(login_form):
	if login_form.admin_id.data != None and login_form.password.data != None:
		return True
	
	return False


class AddVolunteerForm(FlaskForm):
	first_name = StringField("First Name", validators=[DataRequired()])
	last_name = StringField("Last Name",  validators=[DataRequired()])

	admin_password = PasswordField("Your Password", validators=[DataRequired()])
	submit = SubmitField("Create")

def validate_av_form(form):
	if form.first_name.data != None and form.last_name.data != None and form.admin_password.data != None:
		return True
	
	return False
	
class AddAdminForm(FlaskForm):
	first_name = StringField("First Name", validators=[DataRequired()])
	last_name = StringField("Last Name", validators=[DataRequired()])
	
	email = EmailField("Email")
	
	username = StringField("Username", validators=[DataRequired()])
	password = PasswordField("Password", validators=[DataRequired()])
	
	admin_password = PasswordField("My Password", validators=[DataRequired()])
	
	submit = SubmitField("Create Admin")
	
def validate_aa_form(form):
	if form.first_name.data != None and form.last_name.data != None and form.email.data != None and form.username.data != None and form.password.data != None and form.admin_password.data != None:
		return True
	
	return False
	
class AuthAdmin(FlaskForm):
	admin_password = PasswordField("My Password", validators=[DataRequired()])
	submit = SubmitField("Submit")
	
def validate_auth_form(form):
	if form.admin_password.data != None:
		return True
	return False

	
	
	
