from flask import Flask
from flask_login import LoginManager


app = Flask(__name__)
app.config["SECRET_KEY"] = "development"

login = LoginManager(app)

app.secret_key = "development"


import routes
