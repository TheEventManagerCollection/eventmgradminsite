from app import app
from forms import *

from flask import url_for, render_template, redirect, flash, session, request
from flask_login import current_user, login_user, logout_user

from models import *
from EventMgrAPI.User import User
from EventMgrAPI.Volunteer import Volunteer
import traceback

import json

@app.route('/index')
@app.route('/')
def home():
	return render_template('home.html', title="Home")


@app.route('/login', methods=["GET", "POST"])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('home'))
		
	form = LoginForm()
	
	if validate_login_form(form):
		print("Form validated")
		
		admin = FlaskAdmin(int(form.admin_id.data))
		
		if admin == None or not admin.check_password(form.password.data):
			flash("Oops! Please try again.")
			return redirect(url_for('login'))
			
		login_user(admin)
		return redirect(url_for('home'))
	
	return render_template('login.html', title="Log In", form=form)
	
@app.route('/logout')
def logout():
	logout_user()
	
	flash("Logged out successfully!")
	
	return redirect(url_for('home'))
	
@app.route('/add_volunteer', methods=["GET", "POST"])
def add_volunteer():
	form = AddVolunteerForm()
	
	if validate_av_form(form):
		first_name = form.first_name.data
		last_name = form.last_name.data
		
		admin_password = form.admin_password.data
		
		current_user.add_volunteer(first_name, last_name, admin_password)
		
		flash("Added volunteer succesfully!")
		return redirect(url_for("home"))
	
	return render_template("add_volunteer.html", form=form)
		
@app.route('/add_admin', methods=["GET", "POST"])
def add_admin():
	form = AddAdminForm()
	
	
	if validate_aa_form(form):
		
		
		first_name = form.first_name.data
		last_name = form.last_name.data
		
		email = form.email.data
		
		username = form.username.data
		password = form.password.data
		
		admin_password = form.admin_password.data
		
		current_user.add_admin(first_name, last_name, username, email, password, admin_password)
		
		flash("Admin added successfully!")
		return redirect(url_for("home"))
	
	return render_template('add_admin.html', form=form)
	
@app.route('/delete_admin', methods=["GET", "POST"])
def delete_admin():
	
	try:
		if request.form['admin_id'] != None:
			form = AuthAdmin()
			
			
			current_user.delete_admin(Admin(request.form['admin_id']), session.get('admin_password'))
			flash('Admin deleted successfully!')
			
			del session['admin_password']
			
			return redirect(url_for('home'))
			
	except KeyError:
		pass
	
	
	form = AuthAdmin()
	
	if not validate_auth_form(form):
		return render_template("admin_auth.html", form=form)
		
	else:
		admin_password = form.admin_password.data
		session['admin_password'] = admin_password
		
		if current_user.check_password(admin_password):
			admins_json = current_user.get_admins_list(admin_password)
			admins = []
			
			for admin in admins_json:
				
				if not admin['id'] == current_user.id:
					admins.append(admin)
			
			return render_template('delete_admin.html', admins=admins, title="Delete Admin")
			
				
			
		else: 
			return render_template("admin_auth.html", form=form)

@app.route('/view_users', methods=["GET", "POST"])
def view_users():
	
	form = AuthAdmin()
	
	if not validate_auth_form(form):
		return render_template("admin_auth.html", form=form)
		
	else:
		admin_password = form.admin_password.data
		
		if current_user.check_password(admin_password):
			users = current_user.get_users_list(admin_password)
			return render_template('view_users.html', users=users, title="View All Users")
			
				
			
		else: 
			return render_template("admin_auth.html", form=form)		

@app.route('/view_volunteers', methods=["GET", "POST"])
def view_volunteers():
	
	form = AuthAdmin()
	
	if not validate_auth_form(form):
		return render_template("admin_auth.html", form=form)
		
	else:
		admin_password = form.admin_password.data
		
		if current_user.check_password(admin_password):
			volunteers = current_user.get_volunteers_list(admin_password)
			return render_template('view_volunteers.html', volunteers=volunteers, title="View All Volunteers")
			
				
			
		else: 
			return render_template("admin_auth.html", form=form)		

@app.route('/view_admins', methods=["GET", "POST"])
def view_admins():
	
	form = AuthAdmin()
	
	if not validate_auth_form(form):
		return render_template("admin_auth.html", form=form)
		
	else:
		admin_password = form.admin_password.data
		
		if current_user.check_password(admin_password):
			admins = current_user.get_admins_list(admin_password)
			return render_template('view_admins.html', admins=admins, title="View All Volunteers")
			
				
			
		else: 
			return render_template("admin_auth.html", form=form)		





@app.route('/delete_user', methods=["GET", "POST"])
def delete_user():
	print(request.form)
	
	try:
		if request.form['user_id'] != None:	
			print(f"In HERE and session.get('admin_password') is {session.get('admin_password')}")
			
			
			
			re = current_user.delete_volunteer(Volunteer(request.form['volunteer_id']), session.get('admin_password'))
			
			flash('User deleted successfully!')
			
			del session['admin_password']
			
			return redirect(url_for('home'))
			
	except KeyError:
		#print(traceback.format_exc())
		pass
	
	
	form = AuthAdmin()
	
	if not validate_auth_form(form):
		return render_template("admin_auth.html", form=form)
		
	else:
		admin_password = form.admin_password.data
		session['admin_password'] = admin_password
		print(session['admin_password'])
		
		if current_user.check_password(admin_password):
			users = current_user.get_users_list(admin_password)
			return render_template('delete_user.html', users=users, title="Delete Users")
			
				
			
		else: 
			return render_template("admin_auth.html", form=form)		
		
@app.route('/delete_volunteer', methods=["GET", "POST"])
def delete_volunteer():
	print(request.form)
	
	try:
		if request.form['volunteer_id'] != None:	
			print(f"In HERE and session.get('admin_password') is {session.get('admin_password')}")
			
			
			
			re = current_user.delete_volunteer(Volunteer(request.form['volunteer_id']), session.get('admin_password'))
			
			flash('Volunteer deleted successfully!')
			
			del session['admin_password']
			
			return redirect(url_for('home'))
			
	except KeyError:
		#print(traceback.format_exc())
		pass
	
	
	form = AuthAdmin()
	
	if not validate_auth_form(form):
		return render_template("admin_auth.html", form=form)
		
	else:
		admin_password = form.admin_password.data
		session['admin_password'] = admin_password
		print(session['admin_password'])
		
		if current_user.check_password(admin_password):
			volunteers = current_user.get_volunteers_list(admin_password)
			return render_template('delete_volunteer.html', volunteers=volunteers, title="Delete Volunteers")
			
				
			
		else: 
			return render_template("admin_auth.html", form=form)		

		
		
		
